import { StyleSheet } from 'react-native';


export const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF',
    },
    logo: {
        width:'100%',
        margin: 'auto',
        resizeMode: 'contain'
    },
    loginInput:{
      height: 40, 
      borderColor: 'gray', 
      borderWidth: 1
    }
  });
  