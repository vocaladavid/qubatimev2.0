import { UPDATE_CURRENT_ASSIGNMENT, UPDATE_CURRENT_TIMESHEET, UPDATE_CURRENT_DATE, UPDATE_HOURS_FOR_RATE, INCREMENT_CURRENT_DATE, DECREMENT_CURRENT_DATE } from '../actions/assignmentActions';

const initialState = {
    currentAssignmentId: 1,
    allAssignments:{
        1:{
            id:1,
            title: "Bovis Home London",
            agency: "AJP",
            reportongTo: "Sayid Khan",
            currentTimesheetId: 1,
            timesheets:{
                1:{
                    id:1,
                    assignmentId: 1,
                    status: 1,
                    startDate: new Date(),
                    currentDateId: 1,
                    dates:{
                        1:{
                            id:1,
                            date: new Date(2018, 7, 18),
                            status: 1,
                            hours:{
                                1:{
                                    payRateId: 1,
                                    payRateDescription: 'pp rate',
                                    value: 0,
                                },
                                2:{
                                    payRateId: 2,
                                    payRateDescription: 'lkdslks rate',
                                    value: 0,
                                },
                            },
                        },
                        2:{
                            id:2,
                            date: new Date(2018, 7, 19),
                            status: 1,
                            hours:{
                                1:{
                                    payRateId: 1,
                                    payRateDescription: '19 rate',
                                    value: 0,
                                },
                                2:{
                                    payRateId: 2,
                                    payRateDescription: 'weekend rate',
                                    value: 0,
                                },
                                3:{
                                    payRateId: 3,
                                    payRateDescription: 'pp rate',
                                    value: 0,
                                },
                            },
                        },
                        3:{
                            id:3,
                            date: new Date(2018, 7, 20),
                            status: 1,
                            hours:{
                                1:{
                                    payRateId: 1,
                                    payRateDescription: '19 rate',
                                    value: 0,
                                },
                                2:{
                                    payRateId: 2,
                                    payRateDescription: 'weekend rate',
                                    value: 0,
                                },
                                3:{
                                    payRateId: 3,
                                    payRateDescription: 'pp rate',
                                    value: 0,
                                },
                            },
                        },
                        4:{
                            id:4,
                            date: new Date(2018, 7, 21),
                            status: 1,
                            hours:{
                                1:{
                                    payRateId: 1,
                                    payRateDescription: '19 rate',
                                    value: 0,
                                },
                                2:{
                                    payRateId: 2,
                                    payRateDescription: 'weekend rate',
                                    value: 0,
                                },
                                3:{
                                    payRateId: 3,
                                    payRateDescription: 'pp rate',
                                    value: 0,
                                },
                            },
                        },
                        5:{
                            id:5,
                            date: new Date(2018, 7, 22),
                            status: 1,
                            hours:{
                                1:{
                                    payRateId: 1,
                                    payRateDescription: '19 rate',
                                    value: 0,
                                },
                                2:{
                                    payRateId: 2,
                                    payRateDescription: 'weekend rate',
                                    value: 0,
                                },
                                3:{
                                    payRateId: 3,
                                    payRateDescription: 'pp rate',
                                    value: 0,
                                },
                            },
                        },
                        6:{
                            id:6,
                            date: new Date(2018, 7, 23),
                            status: 1,
                            hours:{
                                1:{
                                    payRateId: 1,
                                    payRateDescription: '19 rate',
                                    value: 0,
                                },
                                2:{
                                    payRateId: 2,
                                    payRateDescription: 'weekend rate',
                                    value: 0,
                                },
                                3:{
                                    payRateId: 3,
                                    payRateDescription: 'pp rate',
                                    value: 0,
                                },
                            },
                        },
                        7:{
                            id:7,
                            date: new Date(2018, 7, 24),
                            status: 1,
                            hours:{
                                1:{
                                    payRateId: 1,
                                    payRateDescription: '19 rate',
                                    value: 0,
                                },
                                2:{
                                    payRateId: 2,
                                    payRateDescription: 'weekend rate',
                                    value: 0,
                                },
                                3:{
                                    payRateId: 3,
                                    payRateDescription: 'pp rate',
                                    value: 0,
                                },
                            },
                        },
                    },
                },
                2:{
                    id:2,
                    assignmentId: 1,
                    status: 1,
                    startDate: new Date(),
                    currentDateId: 1,
                    dates:{
                        1:{
                            id:1,
                            date: new Date(2018, 7, 18),
                            status: 1,
                            hours:{
                                1:{
                                    payRateId: 1,
                                    payRateDescription: 's rate',
                                    value: 0,
                                },
                                2:{
                                    payRateId: 2,
                                    payRateDescription: 'test rate',
                                    value: 0,
                                },
                            },
                        },
                    },
                },
            },
        },
        2:{
            id: 2,
            title: "Well well well",
            agency: "Voca",
            reportongTo: "Bobby doo",
        },
    },
};

const setCurrentDateId = (state, dateId) => {
    // check id is valid
    var allDateIds = Object.keys(state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].dates);
    console.log(allDateIds, dateId, allDateIds.indexOf(dateId.toString()))
    if(allDateIds.indexOf(dateId.toString()) < 0){
        return state;
    }

    return Object.assign({}, state, 
        {
            allAssignments:{
                ...state.allAssignments,
                [state.currentAssignmentId]:{
                    ...state.allAssignments[state.currentAssignmentId],
                    timesheets:{
                        ...state.allAssignments[state.currentAssignmentId].timesheets,
                        [state.allAssignments[state.currentAssignmentId].currentTimesheetId]:{
                            ...state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId],
                            currentDateId: dateId,
                        }
                    }
                }
            }
        }
    );
}

const assignmentReducer = (state = initialState, {type, payload}) => {
    switch(type){
        case UPDATE_CURRENT_ASSIGNMENT:
            return Object.assign({}, state, {currentAssignmentId:payload.currentAssignmentId});
        case UPDATE_CURRENT_TIMESHEET:
            return Object.assign({}, state,
                {
                    allAssignments:{
                        ...state.allAssignments,
                        [state.currentAssignmentId]:{
                            ...state.allAssignments[state.currentAssignmentId],
                            currentTimesheetId:payload.currentTimesheetId
                        }
                    }
                }
            );
        case UPDATE_CURRENT_DATE:
            return setCurrentDateId(state, payload.currentDateId);
        case INCREMENT_CURRENT_DATE:
            return setCurrentDateId(state, state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].currentDateId + 1);
        case DECREMENT_CURRENT_DATE:
            return setCurrentDateId(state, state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].currentDateId - 1);
        case UPDATE_HOURS_FOR_RATE:
            return Object.assign({}, state, 
                {
                    allAssignments:{
                        ...state.allAssignments,
                        [state.currentAssignmentId]:{
                            ...state.allAssignments[state.currentAssignmentId],
                            timesheets:{
                                ...state.allAssignments[state.currentAssignmentId].timesheets,
                                [state.allAssignments[state.currentAssignmentId].currentTimesheetId]:{
                                    ...state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId],
                                    dates:{   
                                        ...state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].dates,
                                        [state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].currentDateId]:{
                                            ...state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].dates[state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].currentDateId],
                                            hours:{
                                                ...state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].dates[state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].currentDateId].hours,
                                                [payload.payRateId]:{
                                                    ...state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].dates[state.allAssignments[state.currentAssignmentId].timesheets[state.allAssignments[state.currentAssignmentId].currentTimesheetId].currentDateId].hours[payload.payRateId],
                                                    value: payload.value,
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            );
        default:
            return state;
    }
};

export default assignmentReducer;