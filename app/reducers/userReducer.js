import { UPDATE_USER, UPDATE_USERNAME, UPDATE_PASSWORD } from '../actions/userActions';

const initialState = {
    UserId: 0,
    AuthToken: '',
    Email: '',
    FirstName: '',
    Assignments:{
        Id: 1,
        Title: "Bovis Home London",
        Agency: "AJP",
        ReportongTo: "Sayid Khan",
    }
};

const userReducer = (state = initialState, {type, payload}) => {
    switch(type){
        case UPDATE_USER:
            return Object.assign({}, state, payload.user);
        case UPDATE_USERNAME:
            return Object.assign({}, state, {username:payload.username});
        case UPDATE_PASSWORD:
            return Object.assign({}, state, {password:payload.password});
        default:
            return state;
    }
};

export default userReducer;