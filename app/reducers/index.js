import {combineReducers} from 'redux';

import userReducer from './userReducer';
import assignmentReducer from './assignmentReducer';

export default combineReducers({
    userReducer,
    assignmentReducer,
});