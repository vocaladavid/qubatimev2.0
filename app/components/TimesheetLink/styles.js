import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        width: '100%',
        marginVertical: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    wrapper:{
        marginHorizontal: 5,
        flexDirection:'row',
        justifyContent:'center',
    },
    text:{
        color:'$displayText',
        fontWeight:'600',
    },
    dateText1:{
        fontSize:12,
        margin:0,
        padding:0,
    },
    dateText2:{
        fontSize:16,
        margin:0,
        padding:0,
    },
    dateWrapper:{
        flex:2,
        alignItems: 'center',
        justifyContent:'center',
    },
    timesheet:{
        flex:5,
        justifyContent:'center',
    },
    timesheetTitle:{
        fontSize:14,
    },
    timesheetSubTitle:{
        fontSize:14,
    },
    pendingText:{
        color:'$pendingOrange',
    },
    completeText:{
        color:'$completeGreen',
    },
    borderLeft:{
        height:'100%',
        width:4,
    },
    pendingBackground:{
        backgroundColor:'$pendingOrange',
    },
    completeBackground:{
        backgroundColor:'$completeGreen',
    },
    borderMiddle:{
        height: '80%',
        width:2,
        backgroundColor: '$borderGray',
        marginHorizontal: 20,
    },
    imageWrapper:{
        flex: 1,
    },
    chevron:{
        width:20,
    },
});