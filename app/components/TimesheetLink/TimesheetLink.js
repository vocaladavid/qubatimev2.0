import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Weekdays } from '../../config/constants/global';

import styles from './styles';

const TimesheetLink = ({complete = false, startDate, onPress}) => {

    var timesheetSubTitleStyle = [styles.text, styles.timesheetSubTitle];
    var borderLeftStyle = [styles.borderLeft];
    var subHeaderText = "PENDING";
    if(complete){
        timesheetSubTitleStyle.push(styles.completeText);
        borderLeftStyle.push(styles.completeBackground);
        subHeaderText = "COMPLETE";
    }else{
        timesheetSubTitleStyle.push(styles.pendingText);
        borderLeftStyle.push(styles.pendingBackground);
    }

    var weekday = startDate.getDayName();
    var day = startDate.getDate();
    var month = startDate.getMonthName();

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress}>
                <View style={styles.wrapper}>
                    <View style={borderLeftStyle}></View>
                    <View style={styles.dateWrapper}>
                        <Text style={[styles.text, styles.dateText1]}>
                            {weekday}
                        </Text>
                        <Text style={[styles.text, styles.dateText2]}>
                            {day}
                        </Text>
                        <Text style={[styles.text, styles.dateText1]}>
                            {month}
                        </Text>
                    </View>
                    <View style={styles.borderMiddle}></View>
                    <View style={styles.timesheet}>
                        <Text style={[styles.text, styles.timesheetTitle]}>Current Timesheet</Text>
                        <Text style={timesheetSubTitleStyle}>{subHeaderText}</Text>
                    </View>
                    <View style={styles.imageWrapper}>
                        <Image source={require("./images/chevron-right.png")} style={styles.chevron} resizeMode='contain' />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
};

TimesheetLink.propTypes = {
    complete: PropTypes.bool,
    startDate: PropTypes.any,
    onPress: PropTypes.func,
};

export default TimesheetLink;