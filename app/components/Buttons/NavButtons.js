import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import ClearButton from './ClearButton';
import RoundedButton from './RoundedButton';
import { connect } from 'react-redux';

import styles from './styles';

import { incrementCurrentDateId, decrementCurrentDateId } from '../../actions/assignmentActions';

class NavButtons extends Component {
    render(){
        return(
            <View style={styles.navButtonsContainer}>
                <View style={styles.leftNavButton}>
                    <ClearButton 
                        text="Previous Day"
                        onPress={() => {this.props.dispatch(decrementCurrentDateId());}}
                    />
                </View>
                <View style={styles.rightNavButton}>
                    <RoundedButton 
                        text="Next Day"
                        onPress={() => {this.props.dispatch(incrementCurrentDateId());}}
                    />
                </View>
            </View>
        );
    }
};

NavButtons.propTypes = {
    dispatch: PropTypes.func,
};

export default connect()(NavButtons);