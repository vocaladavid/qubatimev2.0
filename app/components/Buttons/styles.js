import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        alignItems: 'center',
        marginVertical: 5,
    },
    navButtonsContainer:{
        flexDirection:'row',
    },
    leftNavButton:{
        flex:1,
    },
    rightNavButton:{
        flex:1,
    },
    button:{
        width:'70%',
    },
    wrapper:{
        flexDirection: 'row',
        alignItems: 'center',
        padding:8,
    },
    solid: {
        backgroundColor:'$primaryBlue',
    },
    rounded:{  
        borderRadius:28,
    },
    text:{
        color: '$primaryBlue',
        fontSize: 14,
        fontWeight: '600',
        flex:1,
        textAlign:'center',
    },
    white:{
        color:'$white',
    },
});