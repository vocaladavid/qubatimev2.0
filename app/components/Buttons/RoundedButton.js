import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, View, Text } from 'react-native';

import styles from './styles';

const RoundedButton = ({text, onPress}) => {
    
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.button} onPress={onPress}>
                <View style={[styles.wrapper, styles.solid, styles.rounded]}>
                    <Text style={[styles.text, styles.white]}>
                        {text}
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    )
};

RoundedButton.propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func,
};

export default RoundedButton;