import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        alignItems: 'flex-start',
        width:'100%',
    },
    linearGradient: {
        backgroundColor:'$primaryBlue',
        width:'100%',
      },
    pageHeader:{
        fontSize: 20,
        paddingTop: 20,
        color:'$displayText',
        fontWeight: '600',
    },
    subHeader: {
        fontSize: 12,
        paddingBottom: 20,
        color:'$displayText',
    },
    contentTitle:{
        fontSize:12,
        color:'$contentTitleGray',
    },
});