import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import styles from './styles';

const PageHeader = ({text, subHeader}) => {
    return(
        <View style={styles.container}>
            <Text style={styles.pageHeader}>
                {text}
            </Text>
            <Text style={styles.subHeader}>
                {subHeader}
            </Text>
        </View>
    );
};

PageHeader.propTypes = {
    text: PropTypes.string,
    subHeader: PropTypes.string,
};

export default PageHeader;