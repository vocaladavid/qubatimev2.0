import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import styles from './styles';

const ContentTitle = ({text}) => {
    return(
        <View style={styles.container}>
            <Text style={styles.contentTitle}>
                {text}
            </Text>
        </View>
    );
};

ContentTitle.propTypes = {
    text: PropTypes.string,
};

export default ContentTitle;