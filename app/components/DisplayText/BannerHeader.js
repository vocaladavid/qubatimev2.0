import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import styles from './styles';

const BannerHeader = ({text}) => {
    return(
        <View style={styles.container}>
            <Text style={styles.pageHeader}>
                {text}
            </Text>
        </View>
    );
};

BannerHeader.propTypes = {
    text: PropTypes.string,
};

export default BannerHeader;