import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

const CustomCheckbox = ({text, onPress, checked = false}) => {

    var checkboxStyles = [styles.checkbox];
    if(checked){
        checkboxStyles.push(styles.checked);
    }


    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.wrapper} onPress={onPress}>
                <View
                    style={checkboxStyles}
                />
                <Text style={styles.text}>{text}</Text>
            </TouchableOpacity>
        </View>
    );
};

CustomCheckbox.propTypes = {
    text: PropTypes.string,
    checked: PropTypes.bool,
    onPress: PropTypes.func,
};

export default CustomCheckbox;