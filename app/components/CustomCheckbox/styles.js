import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        alignItems:'center',
        marginVertical:10,
    },
    wrapper:{
        flexDirection: 'row',
        width:'70%',
        alignItems: 'center',
        paddingVertical: 10,
        justifyContent:'center',
    },
    checkbox:{
        borderRadius: 2,
        borderWidth: 1,
        width:10,
        height:10,
        borderColor:'$inputText',
    },
    checked:{
        backgroundColor: '#f00',
    },
    text:{
        textAlign: 'center',
        fontSize:12,
        paddingLeft:5,
        color:'$inputText',
    },
});