import React from 'react';
import {View, Image, Text} from 'react-native';

import styles from './styles';

const Logo = () => (
    <View style={styles.container}>
        {/* <Image source={require('../../content/images/dynamiq-logo.png')} /> */}
        <Image source={require('./images/dynamiq-logo.png')} style={styles.image} resizeMode="contain" />
    </View>
);

export default Logo;