import { ContentWidth } from '../../config/constants/global';
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        alignItems:'center',
        paddingVertical: 10,
    },
    image:{
        alignItems: 'center',
        justifyContent: 'center',
        width: ContentWidth,
    }
});