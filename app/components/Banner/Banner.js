import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';

import styles from './styles';

const Banner = ({text, onReturnClick}) => {
    return(
        <View style={styles.container}>
            <View style={styles.linearGradient}>
                <View style={styles.buttonWrapper}>
                    <TouchableOpacity onPress={onReturnClick}>
                        <Text style={styles.text}>X</Text>
                    </TouchableOpacity>
                </View>
                <Text style={styles.pageHeaderWrapper}>
                    <Text style={styles.pageHeader}>
                        {text}
                    </Text>
                </Text>
            </View>
        </View>
    );
};

Banner.propTypes = {
    text: PropTypes.string,
    onReturnClick: PropTypes.func,
};

export default Banner;