import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        alignItems: 'flex-start',
        width:'100%',
    },
    linearGradient: {
        backgroundColor:'$primaryBlue',
        width:'100%',
        flexDirection:'row',
    },
    buttonWrapper:{
        flex:1,
        justifyContent:'center',
        padding:10,
    },
    text:{
        color:'$white',
    },
    pageHeaderWrapper:{
        alignItems:'center',
        justifyContent:'center',
        flex:8,
    },
    pageHeader:{
        fontSize: 20,
        color:'$white',
        fontWeight: '600',
    },
});