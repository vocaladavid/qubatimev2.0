import React from 'react';
import PropTypes from 'prop-types';
import { View, TextInput } from 'react-native';

import styles from './styles';

const BasicInput = (props) => {
    const {editable = true} = props;

    const containerStyles = [styles.container];
    if(editable === false){
        containerStyles.push(styles.containerDisabled);
    }

    return (
        <View style={containerStyles}>
            <TextInput style={styles.input} underlineColorAndroid="transparent" {...props} />
        </View>
    );
};

BasicInput.propTypes = {
    placeholderText: PropTypes.string,
    editable: PropTypes.bool,
};

export default BasicInput;