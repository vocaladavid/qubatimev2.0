import EStyleSheet from 'react-native-extended-stylesheet';
import {StyleSheet} from 'react-native';

const INPUT_HEIGHT = 48;
const BORDER_RADIUS = 4;

export default EStyleSheet.create({
    $buttonBackgroundColorBase: '$white',
    $buttonBackgroundColorModifier: 0.1,

    container:{
        backgroundColor: '$white',
        height: INPUT_HEIGHT,
        borderRadius: BORDER_RADIUS,
        flexDirection: 'row',
        alignItems: 'center',
    },
    containerDisabled:{
        backgroundColor: '$lightGray',
    },
    buttonContainer:{
        height: INPUT_HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:'$white',
        borderTopLeftRadius: BORDER_RADIUS,
        borderBottomLeftRadius: BORDER_RADIUS,
    },
    buttonText:{
        fontWeight: '600',
        fontSize: 12,
        paddingHorizontal: 16,
        color: '$primaryBlue',
    },
    input:{
        height: INPUT_HEIGHT,
        flex:1,
        fontSize: 12,
        lineHeight:10,
        color: '$inputText',
        borderBottomWidth: 1,
        borderBottomColor: '$borderGray',
    },
    border:{
        height: INPUT_HEIGHT,
        width: StyleSheet.hairlineWidth,
        backgroundColor: '$border',
    },
});