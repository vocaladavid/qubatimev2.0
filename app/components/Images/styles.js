import {Dimensions} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        backgroundColor: '#f00',
        alignSelf:'flex-start',
    },
    image:{
        width: Dimensions.get('window').width,
        height:200,
    },
});
