import React from 'react';
import { View, Image } from 'react-native';

import styles from './styles';

const WaveImage = () => {
    return (
            <Image source={require("./images/wave.png")} style={styles.image} resizeMode="contain" />
    );
};

export default WaveImage;