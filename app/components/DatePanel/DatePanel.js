import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import styles from './styles';

import { getCurrentAssignment } from '../../config/helpers/mapStateToPropsHelper';
import { updateCurrentDateId, incrementCurrentDateId, decrementCurrentDateId } from '../../actions/assignmentActions';

class DatePanel extends Component {
    constructor(props){
        super(props);
    }

    onDateSelected(dateId){
        this.props.dispatch(updateCurrentDateId(dateId));
    }

    onPreviousDayPress(){
        this.props.dispatch(decrementCurrentDateId());
    }

    onNextDayPress(){
        this.props.dispatch(incrementCurrentDateId());
    }

    render(){
        return(
            
            <View style={styles.container}>
                <View style={styles.datePanelWrapper}>
                        {Object.values(this.props.dates).map(option => {
                            var dateDayStyles = [styles.dayNumText];
                            if(option.id === this.props.currentDateId){
                                // current date styles
                                dateDayStyles.push(styles.dayNumSelected);
                            }

                            return (
                                <TouchableOpacity style={styles.date} key={option.id} onPress={() => {this.onDateSelected(option.id);}}>
                                    <Text style={styles.dayText}>{option.date.getDayName()}</Text>
                                    <Text style={dateDayStyles}>{option.date.getDate()}</Text>
                                    <View style={styles.dayStatus}></View>
                                </TouchableOpacity>
                            );
                        })}
                </View>
                <View style={styles.currentDateWrapper}>
                    <View style={styles.previousButton}>
                        <TouchableOpacity onPress={() => {this.onPreviousDayPress();}}>
                            <Text>{"<"}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.currentDate}>
                        <Text style={styles.currentDateText}>{this.props.currentDateString}</Text>
                    </View>
                    <View style={styles.nextButton}>
                        <TouchableOpacity onPress={() => {this.onNextDayPress();}}>
                            <Text>{">"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
};

DatePanel.propTypes = {
    currentDateId: PropTypes.number,
    dates: PropTypes.object,
    dispatch: PropTypes.func,
    currentDateString: PropTypes.string,
};

const mapStateToProps = (state) => {
    
    const assignmentSelector = getCurrentAssignment(state);

    const timesheets = assignmentSelector.timesheets || {};
    const timesheetSelector = timesheets[assignmentSelector.currentTimesheetId] || {};
    const dates = timesheetSelector.dates || {};
    
    const currentDateId = timesheetSelector.currentDateId;

    const currentDateObject = dates[currentDateId] || {};
    const currentDate = currentDateObject.date;
    const currentDateString = currentDate!== undefined ? currentDate.toFullDateString() : "";

    return{
        dates,
        currentDateId,
        currentDateString,
    }
};

export default connect(mapStateToProps)(DatePanel);