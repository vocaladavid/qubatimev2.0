import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        alignItems: 'flex-start',
        width:'100%',
    },
    datePanelWrapper:{
        flexDirection:'row',
        marginVertical:20,
    },
    date:{
        flex:1,
        alignItems:'center',
    },
    dayText:{
        color:'$primaryBlue',
    },
    dayNumText:{
        color:'$primaryBlue',
        padding: 5,
        fontWeight: '600',
        marginVertical:2,
    },
    dayNumSelected:{
        backgroundColor: '$displayText',
        color: '$white',
        borderRadius:50,
    },
    dayStatus:{
        width:10,
        height:10,
        backgroundColor:'$pendingOrange',
        borderRadius:50,
    },
    text:{
        color:'$primaryBlue',
    },
    pageHeader:{
        fontSize: 20,
        color:'$white',
        fontWeight: '600',
    },
    currentDateWrapper:{
        flexDirection:'row',
        marginVertical:20,
    },
    currentDateText:{
        color:'$displayText',
    },
    previousButton:{
        flex:1,
        alignItems:'center',
    },
    nextButton:{
        flex:1,
        alignItems:'center',
    },
    currentDate:{
        flex:5,
        alignItems:'center',
    },
});