import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import HourInput from './HourInput';
import {connect } from 'react-redux';
import { updateHoursForRate } from '../../actions/assignmentActions';

import styles from './styles';
import { getCurrentAssignment } from '../../config/helpers/mapStateToPropsHelper';

class HourInputs extends Component {
    constructor(props){
        super(props);
    }
    static propTypes = {
        dispatch: PropTypes.func,
        hours: PropTypes.object,
    };

    render(){
        return(
            <View style={styles.container}>
                {Object.values(this.props.hours).map(option => (
                    <HourInput
                        key={option.id}
                        value={option.value}
                        description={option.payRateDescription}
                        payRateId={option.payRateId}
                        
                        onValueChange={(value) => {
                            this.props.dispatch(updateHoursForRate(option.payRateId, value));
                        }}
                    />
                ))}
            </View>
        );
    }
};

const mapStateToProps = (state) => {
    
    const assignmentSelector = getCurrentAssignment(state);

    const timesheets = assignmentSelector.timesheets || {};
    const timesheetSelector = timesheets[assignmentSelector.currentTimesheetId] || {};
    const dates = timesheetSelector.dates || {};
    const dateSelector = dates[timesheetSelector.currentDateId] || {};

    const hours = dateSelector.hours || {};

    return {
        hours,
    }
};

// const mapStateToProps = (state) => {
//     const timesheetSelector = state.timesheetReducer.allTimesheets.filter(x => x.id === state.timesheetReducer.currentTimesheetId)[0] || {};
//     const dates = timesheetSelector.dates;
//     const dateSelector = dates.filter(x => x.id === timesheetSelector.currentDateId)[0] || {};

//     const hours = dateSelector.hours;

//     return {
//         hours,
//     }
// };

export default connect(mapStateToProps)(HourInputs);