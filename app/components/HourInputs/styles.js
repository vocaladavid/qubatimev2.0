import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{
        alignItems: 'flex-start',
        width:'100%',
    },
    inputWrapper:{
        paddingVertical:10,
        width:'100%',
        alignItems:'center',
    },
    slider:{
        width:'90%',
        height:20,
    },
    sliderTextWrapper:{
        width:'80%',
        flexDirection: 'row',
    },
    hourTextWrapper:{
        flex:2,
    },
    displayText:{
        color:'$displayText',
    },
    hourDisplayText:{
        color:'$primaryBlue',
    },
    divider:{
        width:2,
        height:'100%',
        backgroundColor:'$borderGray',
    },
    payRateNameWrapper:{
        flex:3,
        paddingHorizontal:10,
    },
});