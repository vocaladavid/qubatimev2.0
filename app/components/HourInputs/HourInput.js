import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { View, Slider, Text, TouchableOpacity } from 'react-native';

import styles from './styles';

class HourInput extends Component {
    constructor(props){
        super(props);
        // this.State = {
        //     modalVisible: false,
        //   }
    }
    state = {
        value: 0,
    }
    static propTypes = {
        key: PropTypes.number,
        payRateId: PropTypes.number,
        value: PropTypes.number,
        description: PropTypes.string,
        onValueChange: PropTypes.func,
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.inputWrapper}>
                    <View style={styles.sliderTextWrapper}>
                        <View style={styles.hourTextWrapper}>
                            <Text style={styles.hourDisplayText}>{this.props.value}</Text>
                        </View>
                        
                        <View style={styles.divider}></View>
                        
                        <View style={styles.payRateNameWrapper}>
                            <Text style={styles.displayText}>{this.props.description}</Text>
                        </View>
                    </View>
                    <Slider 
                        step={1}
                        maximumValue={24}
                        value={this.props.value}
                        style={styles.slider}
                        minimumTrackTintColor="#009bce"
                        maximumTrackTintColor="#3b5467"
                        thumbTintColor="#009bce"
                        onValueChange={this.props.onValueChange}
                    />
                </View>
            </View>
        );
    }
};

export default HourInput;