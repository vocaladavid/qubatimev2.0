import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
    container:{

    },
    button:{
        width: '100%',
    },
    wrapper:{
        flexDirection:'row',
        width:'100%',
    },
    textWrapper:{
        flex:5,
    },
    imageWrapper:{
        flex:1,
    },
    chevron:{
        width:25,
    },
    modelWrapper:{
        backgroundColor:'red',
    },
});