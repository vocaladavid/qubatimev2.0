import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Modal, View, Text, Image, TouchableOpacity, TouchableHighlight } from 'react-native';
import {connect} from 'react-redux';

import {updateCurrentAssignmentId} from '../../actions/assignmentActions';

import styles from './styles';

class DropDown extends Component {
    constructor(props){
        super(props);
        // this.State = {
        //     modalVisible: false,
        //   }
    }
    state = {
        modalVisible: false,
        name: 'Bovis Home London',
        agency: '',
        reportingTo: '',
    };

    componentDidMount(){
        //this.props.dispatch(updateTitle("what"));
    }

    setModalVisible(visible) {
    this.setState({modalVisible: visible});
    }

    updateSelection(id) {
        this.props.dispatch(updateCurrentAssignmentId(id));
    }
        
  render(){
    return (
        <View style={styles.container}>
            <Modal
                animationType="fade"
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                Alert.alert('Modal has been closed.');
            }}>
          <View style={styles.modalWrapper}>
            <View>
                {Object.values(this.props.allAssignments).map(option => (
                    
              <TouchableOpacity
              key={option.id}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
                this.updateSelection(option.id);
              }}>
              <Text>{option.title}</Text>
            </TouchableOpacity>
                ))}
            </View>
          </View>
        </Modal>
            <TouchableOpacity style={styles.button} onPress={()=>{
                this.setModalVisible(!this.state.modalVisible);
            }}>
                <View style={styles.wrapper}>
                    <View style={styles.textWrapper}>
                        <Text>{this.props.assignmentTitle}</Text>
                        <Text>Current Agency: {this.props.assignmentAgency}</Text>
                        <Text>Reporting to: {this.props.assignmentReportingTo}</Text>
                    </View>
                    <View style={styles.imageWrapper}>
                        <Image source={require("./images/chevron-down.png")} style={styles.chevron} resizeMode='contain' />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
}
};

DropDown.propTypes = {
    dispatch: PropTypes.func,
    options: PropTypes.array,
    assignmentTitle: PropTypes.string,
    assignmentAgency: PropTypes.string,
    assignmentReportingTo: PropTypes.string,
    allAssignments: PropTypes.object,
};

const mapStateToProps = (state) => {
    
    const allAssignments = state.assignmentReducer.allAssignments;
    const assignmentSelector = allAssignments[state.assignmentReducer.currentAssignmentId] || {};

    const assignmentTitle = assignmentSelector.title;
    const assignmentAgency = assignmentSelector.agency;
    const assignmentReportingTo = assignmentSelector.reportongTo;

    return{
        assignmentTitle,
        assignmentAgency,
        assignmentReportingTo,
        allAssignments,
    }
};


export default connect(mapStateToProps)(DropDown);