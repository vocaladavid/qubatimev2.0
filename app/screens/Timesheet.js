import React, { Component } from 'react';
import { StatusBar, View, Text } from 'react-native';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';

import { Container } from '../components/Container';
import { Banner } from '../components/Banner';
import { DatePanel } from '../components/DatePanel';
import { HourInputs } from '../components/HourInputs';
import { NavButtons } from '../components/Buttons';


class Timesheet extends Component{
    constructor(props){
        super(props);
    }
    static propTypes = {
        navigation: PropTypes.object,
    };

    render(){
        return (
            <Container>
                <StatusBar translucent={false} barStyle="light-content" />

                <Banner 
                    text="Current Timesheet"
                    onReturnClick={() => {this.props.navigation.goBack();}}
                />
                <DatePanel />
                <HourInputs />
                <NavButtons />

            </Container>

        );
    }
}

const mapStateToProps = (state) => {
    // const timesheetSelector = state.timesheetReducer.allTimesheets.filter(x => x.id === state.timesheetReducer.currentTimesheetId)[0] || {};
    // const dates = timesheetSelector.dates;

    return{
        //dates,
    }
};

export default connect(mapStateToProps)(Timesheet);