import React, { Component } from 'react';
import { StatusBar, KeyboardAvoidingView, AsyncStorage } from 'react-native';

import PropTypes from 'prop-types';

import { Container } from '../components/Container';
import { Logo } from '../components/Logo';
import { BasicInput } from '../components/TextInput';
import { ClearButton, RoundedButton } from '../components/Buttons';
import { CustomCheckbox } from '../components/CustomCheckbox';
import { WaveImage } from '../components/Images';

import { connect } from 'react-redux';

import {updateUsername} from '../actions/userActions';

class Login extends Component{

    constructor(props){
        super(props);
        
        this.state = { checked: false };
    }
    static propTypes = {
        navigation: PropTypes.object,
        dispatch: PropTypes.func,
    };

    handleTextChange = (text) => {
        console.log('change text', text);
        this.props.dispatch(updateUsername(text));
    };

    handleForgottonPassword = () => {
        console.log("forgotton password");
    };

    handleLogin = () => {
        console.log("Login");
        
        
        this.props.navigation.navigate('Dashboard');
        // fetch('https://qubatimesheetsapi.azurewebsites.net/api/Account', {
        //     method: 'POST',
        //     headers: {
        //         Accept: 'application/json',
        //         'Content-Type': 'application/json',
        //     },
        //     body: JSON.stringify({
        //         Email:'approver@nxtds.com',
        //         password:"websolo99"
        //     }),
        // })
        // .then((response) => response.json())
        // .then((responseJson) => {
        //     console.log(responseJson);
        //     if(responseJson.Authorised === true){
        //         AsyncStorage.setItem('userId', responseJson.UserId);
        //         AsyncStorage.setItem('authToken', responseJson.UserId);
                
        //         this.props.navigation.navigate('Dashboard');
        //     }
        // });
    };

    toggleRememberMe = () => {
        this.setState({
            checked: !this.state.checked
          });
    };

    render(){
        return (
            <Container>
                <StatusBar translucent={false} barStyle="light-content" />

                <KeyboardAvoidingView behavior="padding">
                    <Logo />
                    
                    <BasicInput 
                        // keyboardType="numeric"
                        placeholder="Email"
                        placeholderTextColor="#009bce"
                        onChangeText={this.handleTextChange}
                    />
                    <BasicInput 
                        placeholder="Password"
                        placeholderTextColor="#009bce"
                    />

                    <CustomCheckbox 
                        text="Remember Me"
                        checked={this.state.checked}
                        onPress={this.toggleRememberMe}
                    />

                    <RoundedButton 
                        text="Login"
                        onPress={this.handleLogin}
                    />
                    <ClearButton 
                        text="Forgotten Password"
                        onPress={this.handleForgottonPassword}
                    />

                </KeyboardAvoidingView>
                
                <WaveImage />

            </Container>
        );
    }
}

export default  connect()(Login);