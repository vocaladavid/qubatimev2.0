import React, { Component } from 'react';
import { StatusBar, View, Text, AsyncStorage } from 'react-native';
import {connect} from 'react-redux';

import PropTypes from 'prop-types';

import { Container } from '../components/Container';
import { ClearButton, RoundedButton } from '../components/Buttons';
import { PageHeader, ContentTitle } from '../components/DisplayText';
import { DropDown } from '../components/DropDowns';
import { TimesheetLink } from '../components/TimesheetLink';

import {updateCurrentTimesheetId} from '../actions/assignmentActions';
import { getCurrentAssignment } from '../config/helpers/mapStateToPropsHelper';


class Dashboard extends Component{

    constructor(props){
        super(props);
    }
    static propTypes = {
        dispatch: PropTypes.func,
        navigation: PropTypes.object,
        email: PropTypes.string,
        timesheets: PropTypes.object,
    };

    componentDidMount(){

        // get user timesheet data
        // var userId = 2750;
        // AsyncStorage.getItem("userId").then(value => {
        //     userId = value;
        // });
        // var authToken = 'aitFM2hHZTV5QXB0OU9OR2JudzNEaHd0ZWE2RTBRVlBjL2V1a2ZBckl0WHVHdG8wMVZCWFNMcEE0QkpweWpkREk2bERrelozRzNueWU3dlZhL2o1MDhVUmpIWEJYU3Q5SHIraTNhWTVWZEg5RDJOZllYWmlSZTRIOG5ZVzFJK2FheUtNVFgzMzhmRTVCS0xvMmlWVzhRPT0=';
        // AsyncStorage.getItem("authToken").then(value => {
        //     authToken = value;
        // });

        // fetch('https://qubatimesheetsapi.azurewebsites.net/api/values/1', {
        //     method: 'GET',
        //     headers: {
        //         Accept: 'application/json',
        //         'Content-Type': 'application/json',
        //         Authorization: 'Basic ' + authToken,
        //         UserId: userId,
        //     },
        // })
        // .then((response) => response.json())
        // .then((responseJson) => {
        //     console.log(responseJson);
        // });
    }

    render(){
        return (
            <Container>
                <StatusBar translucent={false} barStyle="light-content" />

                <PageHeader 
                    text="Jonathan"
                    subHeader={this.props.email}
                />

                <ContentTitle 
                    text="Your Assignments"
                />

                <DropDown />

                <ContentTitle 
                    text="Your Timesheets"
                />

                {Object.values(this.props.timesheets).map(option => (
                    
                        <TimesheetLink 
                            key={option.id}
                            complete={false}
                            startDate ={option.startDate}
                            onPress={() => {
                                this.props.dispatch(updateCurrentTimesheetId(option.id));
                                this.props.navigation.navigate('Timesheet');
                            }}
                        />
                ))}

                <ClearButton 
                    text="View All Timesheets"
                    onPress={this.handleForgottonPassword}
                />
            </Container>

        );
    }
}

const mapStateToProps = (state) => {
    const email = state.userReducer.username;

    const assignmentSelector = getCurrentAssignment(state);

    const timesheets = assignmentSelector.timesheets || {};

    return{
        email,
        timesheets,
    }
};

export default connect(mapStateToProps)(Dashboard);