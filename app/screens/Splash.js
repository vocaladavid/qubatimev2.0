import React, { Component } from 'react';
import { StatusBar, Text, AsyncStorage } from 'react-native';
import PropTypes from 'prop-types';

import { Container } from '../components/Container';
import { Logo } from '../components/Logo';

class Splash extends Component{

    constructor(props){
        super(props);
        
        this.state = { checked: false };
    }
    static propTypes = {
        navigation: PropTypes.object,
    };


    componentDidMount(){
        AsyncStorage.getItem("userToken").then(value => {
            if(value == null){
                 AsyncStorage.setItem('alreadyLaunched', true); // No need to wait for `setItem` to finish, although you might want to handle errors
                 this.setState({firstLaunch: true});
                 this.props.navigation.navigate('Login');
            }
            else{
                 this.setState({firstLaunch: false});
                 this.props.navigation.navigate('Dashboard');
            }}) // Add some error handling, also you can simply do this.setState({fistLaunch: value == null})
    }

    render(){
        return (
            <Container>
                <StatusBar translucent={false} barStyle="light-content" />

                    <Logo />
                    
                    <Text>Splash screen</Text>

            </Container>
        );
    }
}

export default Splash;