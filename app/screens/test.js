import React, { Component } from 'react';
import {View, KeyboardAvoidingView, Image, TextInput, Button} from 'react-native';
import { connect } from 'react-redux';
import { updateUsername, updatePassword, apiRequest, loginApiRequest} from '../actions/userActions';
import {createSelector} from 'reselect';
import {styles} from '../content/styles/loginStyles';


class Login extends Component {
    constructor(props){
        console.log(props);
      super(props);
  
      this.onUpdateUsername = this.onUpdateUsername.bind(this);
      this.onUpdatePassword = this.onUpdatePassword.bind(this);
    }
  
    static navigationOptions = {
        title: 'Login',
      };

    componentDidMount(){
        //this.props.onApiRequest();
    }

    onUpdateUsername(newVal) {
        this.props.onUpdateUsername(newVal);
    }

    onUpdatePassword(newVal) {
        this.props.onUpdatePassword(newVal);
    }
      
    onLoginSubmit = () => {
        console.log(this.props);
        this.props.onLoginApiRequest(this.props.user.username, this.props.user.password);
    }
    
    render(){
        return(
          <KeyboardAvoidingView>
            <Image source={require('../content/images/dynamiq-logo.png')} style={styles.logo} />
            
            <TextInput onChangeText={this.props.onUpdateUsername} placeholder="Username"  style={styles.loginInput}>{this.props.user.username}</TextInput>
    
            <TextInput onChangeText={this.props.onUpdatePassword} placeholder="Password" style={styles.loginInput}>{this.props.user.password}</TextInput>
    
            <Button title='Login' onPress={this.onLoginSubmit} />
    
          </KeyboardAvoidingView>
        )
    };
};

const productsSelector = createSelector(
    state => state.products,
    products => products
);
  
const userSelector = createSelector(
    state => state.user,
    user => user
);

const mapStateToProps = createSelector(
    productsSelector,
    userSelector,
    (products, user) => ({
        products,
        user
    })
);

  const mapActionsToProps =  {
        onUpdateUsername: updateUsername,
        onUpdatePassword: updatePassword,
        onLoginApiRequest: loginApiRequest,
        onApiRequest: apiRequest
  };

  export default connect(mapStateToProps, mapActionsToProps)(Login);
