import { Dimensions } from 'react-native';
import '../extensions/dateExt';

const ContentWidth = Dimensions.get('window').width * 0.75;

export { ContentWidth };