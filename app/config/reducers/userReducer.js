import { UPDATE_USER, UPDATE_USERNAME, UPDATE_PASSWORD } from '../../actions/userActions';

export default function userReducer(state = '', {type, payload}){
    switch(type){
        case UPDATE_USER:
            return Object.assign({}, state, payload.user);
        case UPDATE_USERNAME:
            return Object.assign({}, state, {username:payload.username});
        case UPDATE_PASSWORD:
            return Object.assign({}, state, {password:payload.password});
        default:
            return state;
    }
}