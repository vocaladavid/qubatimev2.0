import moment from 'moment';


Date.prototype.getDayName = function(){
    
    var weekdays = new Array(7);
    weekdays[0] = "SUN";
    weekdays[1] = "MON";
    weekdays[2] = "TUE";
    weekdays[3] = "WED";
    weekdays[4] = "THU";
    weekdays[5] = "FRI";
    weekdays[6] = "SAT";
    var weekday = weekdays[this.getDay()];

    return weekday;
}

Date.prototype.getMonthName = function(){
    
    var months = new Array(12);
    months[0] = "JAN";
    months[1] = "FEB";
    months[2] = "MAR";
    months[3] = "APR";
    months[4] = "MAY";
    months[5] = "JUN";
    months[6] = "JUL";
    months[7] = "AUG";
    months[8] = "SEP";
    months[9] = "OCT";
    months[10] = "NOV";
    months[11] = "DEC";
    var month = months[this.getMonth()];

    return month;
}

Date.prototype.toFullDateString = function(){
    
    var momentDate = moment(this);

    return momentDate.format('dddd Do MMMM YYYY');
}
