
import {createStackNavigator} from 'react-navigation';
import { Dashboard, Timesheet, Login, Splash } from '../screens';


export default createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions:{
            header: () => null,
        },
    },
    Splash: {
        screen: Splash,
        navigationOptions:{
            header: () => null,
        },
    },
    Timesheet: {
        screen: Timesheet,
        navigationOptions:{
            header: () => null,
        },
    },
    Dashboard: {
        screen: Dashboard,
        navigationOptions:{
            header: () => null,
        },
    },
});
