

export const getCurrentAssignment = (state) => {        
    const allAssignments = state.assignmentReducer.allAssignments;
    const assignmentSelector = allAssignments[state.assignmentReducer.currentAssignmentId] || {};

    return assignmentSelector;
};
