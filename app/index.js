
import React, { Component } from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';

import { applyMiddleware, compose, combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { styles } from './content/styles/globalStyles';
//import logger from 'redux-logger';

import Navigator from './config/routes';

import store from './config/store';

EStyleSheet.build({
    $primaryBlue: '#009bce',
    $white: '#FFFFFF',
    $border: '#E2E2E2',
    $inputText:'#797979',
    $lightGray: '#F0F0F0',
    $borderGray: '#F0F0F0',
    $displayText: '#3b5467',
    $contentTitleGray: '#c9c9c9',
    $pendingOrange: '#fa9f42',
    $completeGreen: '#7bc950',
});

export default class App extends Component {
  render(){
      return(
           <Provider store={ store } styles={styles.container}>
                <Navigator />
           </Provider>
      )
  };
};


