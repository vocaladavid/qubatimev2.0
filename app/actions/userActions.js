
//import $ from 'jquery';


export const UPDATE_USER = 'users:updateUser';
export const UPDATE_USERNAME = 'users:updateUsername';
export const UPDATE_PASSWORD = 'users:updatePassword';

export function updateUser(user){
    return {
        type: UPDATE_USER,
        payload: {
            user:user
        }
    }
}

export function updateUsername(val){
    return {
        type: UPDATE_USERNAME,
        payload: {
            username:val
        }
    }
}

export function updatePassword(val){
    return {
        type: UPDATE_PASSWORD,
        payload: {
            password:val
        }
    }
}

export function loginApiRequest(email, password){
    return dispatch => {
        console.log("login request");
        fetch('https://qubatimesheetsapi.azurewebsites.net/api/login',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({Email: email, Password:password})
        })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        var user = {
            username: responseJson.Email,
            name: responseJson.Name,
            userType: responseJson.UserType,
            authorised: responseJson.Authorised,
            authToken: responseJson.AuthToken
        }
        dispatch(updateUser(user));
      })
      .catch((error) =>{
        console.error(error);
      });
    }
}
export function apiRequest(){
    return dispatch => {
        // $.ajax({
        //     url:'https://qubatimesheetsapi.azurewebsites.net/api/timesheets/1234098765penny30030303030303030303030030',
        //     success() {
        //         console.log('SUCCESS');
        //     },
        //     error() {
        //         console.log('ERROR');
        //     }
        // });
        fetch('https://qubatimesheetsapi.azurewebsites.net/api/timesheets/1234098765penny30030303030303030303030030')
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        if(responseJson.QubaTimesheets !== undefined){
            dispatch(updateUsername(responseJson.QubaTimesheets[0].Worker));
        }
      })
      .catch((error) =>{
        console.error(error);
      });
    }
}
