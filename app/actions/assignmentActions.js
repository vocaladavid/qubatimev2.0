
//import $ from 'jquery';


export const UPDATE_CURRENT_ASSIGNMENT = 'assignment:updateCurrentAssignmentId';
export const UPDATE_CURRENT_TIMESHEET = 'assignment:updateCurrentTimesheetId';
export const UPDATE_HOURS_FOR_RATE = 'assignment:updateHoursForRate';
export const UPDATE_CURRENT_DATE = 'assignment:updateCurrentDateId'
export const INCREMENT_CURRENT_DATE = 'assignment:incrementCurrentDateId'
export const DECREMENT_CURRENT_DATE = 'assignment:decrementCurrentDateId'

export function updateCurrentAssignmentId(currentAssignmentId){
    return {
        type: UPDATE_CURRENT_ASSIGNMENT,
        payload: {
            currentAssignmentId
        }
    }
}

export function updateCurrentTimesheetId(currentTimesheetId){
    return {
        type: UPDATE_CURRENT_TIMESHEET,
        payload: {
            currentTimesheetId
        }
    }
}


export function updateCurrentDateId(currentDateId){
    return {
        type: UPDATE_CURRENT_DATE,
        payload: {
            currentDateId
        }
    }
}



export function incrementCurrentDateId(){
    return {
        type: INCREMENT_CURRENT_DATE,
        payload: {}
    }
}



export function decrementCurrentDateId(){
    return {
        type: DECREMENT_CURRENT_DATE,
        payload: {}
    }
}


export function updateHoursForRate(payRateId, value){
    return {
        type: UPDATE_HOURS_FOR_RATE,
        payload: {
            payRateId,
            value,
        }
    }
}


